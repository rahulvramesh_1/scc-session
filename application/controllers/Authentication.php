<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Authentication extends CI_Controller {

	public function index()
	{

	}

  public function login()
  {
   $this->load->view('header');
   $this->load->view('Authentication/login');
   $this->load->view('footer');
  }

  public function doLogin()
  {
    $this->load->model('auth_login');
    $username = $this->input->post('username');
    $password = $this->input->post('password');

    $user = $this->auth_login->checkLogin($username,$password);

    if($user)
    {
      print_r($user);
    }
    else{
      echo "Please Login";
    }


  }
}
